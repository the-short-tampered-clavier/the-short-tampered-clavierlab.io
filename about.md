---
layout: page
title: About
---

**Aalok Sathe** I am a student at the
[University of Richmond, VA](http://richmond.edu). I'm a computer science
major and might end up also majoring in cognitive science or math.
I also study linguistics.
I'm interested in math, machines, linguistics, philosophy, neuroscience,
football (soccer) and music (in no particular order), among other things. I'm a
FOSS enthusiast. I have undergone training in Indian classical music as a
vocalist, and have been learning Western voice at UR, where I also sing in the
University mixed choir.
I find organization within nature, and systems of all kinds, fascinating -- the
complex human brain, computers, human society, the universe, to name a few.
For more, you can visit my [webpage](https://aalok-sathe.gitlab.io).

**Mandar Juvekar** I'm a 12th grade student in Pune, India. My interests include
mathematics, computer science, and Western and Indian classical music. I am a
Google Code-In top 10 contributor for the
[MovingBlocks](https://github.com/MovingBlocks) open source organization
and have been actively involved in their [Terasology](http://terasology.org)
project since then. I am currently working as a mentor for the organization for
the [Google Summer of Code](https://summerofcode.withgoogle.com) competition. I
have been training as a classical pianist for 6 years, have passed the
Trinity College of London's piano exams up to grade 5 with distinction, and am
currently working on the Associate of the Trinity College of London (ATCL)
diploma in piano performance. I can also play the tabla, and have been receiving
informal education in Hindustani classical music from Shardul.

**Shardul Chiplunkar** I'm a high-school senior originally from Pune, India, and
currently in Fremont, CA. My interests include mathematics, computer science,
linguistics, and both Hindustani/North Indian and Western classical music. I've
done a fair bit of competitive mathematics, not too successfully, but I tinker
around a lot with mathematics anyway---I attended
[MathILy-Er]({{ site.baseurl }}{% post_url 2017-07-07-hello-from-mathilyer %})
in July 2017 and I'm a semi-active member of
[Mathematics Stack Exchange](https://math.stackexchange.com/users/140607/shardulc).
I am a
[Google Code-In Grand Prize Winner](https://opensource.googleblog.com/2017/01/announcing-google-code-in-2016-winners.html)
for my work with [Apertium](https://www.apertium.org/), an open source machine
translation platform. I've been on Team India for the
[14th International Linguistics Olympiad](http://iol14.plo-in.org/)
with Aalok as a senior teammate,
and also on the [15th](http://www.iol2017.ie/) where I won an honorable mention.
I have been training as a
Hindustani classical vocalist for 10 years and have rudimentary tabla and
harmonium skills, and I have been receiving an informal education in Western
classical music from Mandar.
